    # What is this repository for?

    * Nightsbridge Gym booking assessment backend
    * Version: 1.0.0


    ## Tech stack
    
    ### Language
    * [Java 8](https://java.com/en/download/help/java8.html)
    
    ### Framework
    * [spring-boot](https://spring.io/projects/spring-boot)
    
    ### Database
    * [H2 in-memory database](https://www.h2database.com/html/main.html)    

    ### Build tool
    * [gradle](https://gradle.org)
    
    ### Other libraries
    * [Hibernate ORM](https://hibernate.org/)
    * [Querydsl](http://www.querydsl.com/)
    * [JUnit 5](https://junit.org/junit5/)
    * [Mockito](https://site.mockito.org/)
    * [Spring Rest Docs](https://spring.io/projects/spring-restdocs)
    * [Asciidoctor](https://asciidoctor.org/)

    ## How do I get set up?

    ### How to build
    * After cloning cd into project directory and run ./gradlew build
    ### How to run
    * To run after building run ./gradlew bootrun
    ### How to run tests
    * run ./gradlew test (tests will also run when running ./gradlew build)
    ### How to access and query the database
    * After starting the application Navigate to [h2 database](https://localhost:8111/h2-console/)
    * The default credentials are:
    * username: sa
    * password: 
    * (Password is empty string).
    * click on connect to get to the console page.
    ### Documentation
    * I documented some of the application code using javadoc however I still have quite a lot to document.
    * I documented part of the REST API using Spring Rest Docs and Asciidoctor to convert to html.
    * The API documentation is based on unit tests: I.E. If the unit tests passes snippets gets generated and the asciidoc file gets converted to HTML.
    * to view the API documentation: after running a build navigate to <projectRootDir>/build/docs/asciidoc and open index.html in a browser.

    ## Assumptions/noteworthy mentions #
    ### Given the requirements I made the following assumptions which might differ slightly from what you expected:
    * A customer makes a booking for a one on one class/session with a trainer at a specified date/time as apposed to many customers booking one class with a constant daily start time.
    * I had very limited time over the past two weeks and therefore couldn't polish the UI as much as I would have liked however I think all required functionality is implemented...
    * I implemented filtering classes by trainer however didn't manage get to classifying gym class types and filtering by type as well.
    * My laptop is completely locked down and i'm only able to navigate to unfiltered web URLs and I don't have Visio installed therefore I couldn't draw ERD or UML/class diagrams however relationships are specified below.
    * I added a Insomnia Rest client collection located at <projectDir>/etc/insomnia_rest_client_collection.json with all endpoints configured, this file must be imported into Insomnia or Postman.

     ### Code smells/bad practices 
    * As there are no user management/authentication or authorization implemented as part of this assessment I hardcoded a default test user in the UserServiceImpl class.
    * I also hardcoded fetching the default user when booking a class as apposed to parameterising the method signature.
    * I am aware that this is very bad practice especially in the context of testing however I thought it would be OK if I noted this here (in the context of this only beeing an assessment).

    ## Domain relationships/design
    * A customer can make multiple bookings and one booking maps back to one customer, i,e. booking -> customer = many to one and customer -> booking = one to many.
    * A trainer can have multiple bookings and one booking maps back to one trainer, i.e. booking -> trainer = many to one and trainer -> booking = one to many.
    * A trainer can give multiple classes and one gym class maps back to one trainer, i.e. gymClass -> trainer = many to one and trainer -> gymClass = one to many.
    * a gymClass can have multiple bookings (not in the same timeslot, I manage this in code) and one booking maps back to one gym class, i.e. booking -> gymClass = Many to one and gymClass -> booking = one to many
