package com.nightsbridge.gym;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NbGymBoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NbGymBoApplication.class, args);
	}

}
