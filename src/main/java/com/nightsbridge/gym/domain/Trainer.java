package com.nightsbridge.gym.domain;

import org.hibernate.annotations.Generated;

import javax.persistence.*;
import java.util.List;

@Entity
public class Trainer extends Person {

    @OneToMany(mappedBy = "trainer")
    private List<GymClass> gymClasses;

    @OneToMany(mappedBy = "trainer")
    private List<Booking> bookings;

    public Trainer(String firstName, String lastName, int age, String email) {
        super(firstName, lastName, age, email);
    }

    public Trainer() {
        super();
    }

}
