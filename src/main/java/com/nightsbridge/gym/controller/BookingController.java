package com.nightsbridge.gym.controller;

import com.nightsbridge.gym.dto.BookingDto;
import com.nightsbridge.gym.service.BookingServiceImpl;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Future;
import java.time.LocalDateTime;
import java.util.List;

@Validated
@RestController
public class BookingController {
    private BookingServiceImpl bookingService;

    public BookingController(BookingServiceImpl bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping("/bookings/{userId}")
    public List<BookingDto> getAllBookings(@PathVariable Long userId) {
        return bookingService.getAllBookings(userId);
    }

    @PostMapping("/nb/bookings/create")
    public BookingDto createBooking(@RequestParam Long gymClassId, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @Future(message = "Booking must be in the future") LocalDateTime bookingStartTime) {
        return bookingService.createBooking(gymClassId, bookingStartTime);
    }

}


