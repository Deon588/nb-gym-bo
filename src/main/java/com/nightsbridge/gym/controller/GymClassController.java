package com.nightsbridge.gym.controller;

import com.nightsbridge.gym.dto.GymClassDto;
import com.nightsbridge.gym.service.GymClassService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * * GymClassController exposes all Gym class related operations
 */
@RestController
public class GymClassController {
    private final GymClassService gymClassService;

    public GymClassController(GymClassService gymClassService) {
        this.gymClassService = gymClassService;
    }

    /**
     *this method is used to fetch all gym classes
     * @return A list of all gym classes
     */
    @GetMapping("/nb/gymclasses")
    public List<GymClassDto> getAllGymClasses() {
        return gymClassService.getAllGymClasses();
    }

    /**
     *This method is used to get a filtered list of gym classes     *
     * @param trainerId The ID of the trainer that's filtered on
     * @return A filtered list of GymclassDtos
     */
    @GetMapping("/nb/gymclasses/search/{trainerId}")
    public List<GymClassDto> searchGymClasses(@PathVariable Long trainerId) {
        return gymClassService.searchGymClasses(trainerId);
    }
}
