package com.nightsbridge.gym.controller;

import com.nightsbridge.gym.dto.TrainerDto;
import com.nightsbridge.gym.service.TrainerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TrainerController {
    private final TrainerService trainerService;

    public TrainerController(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @GetMapping("/nb/trainers")
    public List<TrainerDto> getAllTrainers() {
        return trainerService.getAllTrainers();
    }
}
