package com.nightsbridge.gym;

import com.nightsbridge.gym.domain.Booking;
import com.nightsbridge.gym.domain.Customer;
import com.nightsbridge.gym.domain.GymClass;
import com.nightsbridge.gym.domain.Trainer;
import com.nightsbridge.gym.repository.BookingRepository;
import com.nightsbridge.gym.repository.CustomerRepository;
import com.nightsbridge.gym.repository.GymClassRepository;
import com.nightsbridge.gym.repository.TrainerRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Random;

@Component
public class LoadTestData {
    private CustomerRepository customerRepository;
    private TrainerRepository trainerRepository;
    private GymClassRepository gymClassRepository;
    private BookingRepository bookingRepository;

    public LoadTestData(CustomerRepository customerRepository, TrainerRepository trainerRepository, GymClassRepository gymClassRepository, BookingRepository bookingRepository) {
        this.customerRepository = customerRepository;
        this.trainerRepository = trainerRepository;
        this.gymClassRepository = gymClassRepository;
        this.bookingRepository = bookingRepository;
    }

    @PostConstruct
    public void execute() {
        Customer c1 = createCustomer("Deon", "nel");
        Trainer t1 = createTrainer("Usain", "Bolt");
        Trainer t2 = createTrainer("Tyreek", "Hill");
        Trainer t3 = createTrainer("Leonard", "Fournette");

        GymClass g1 = createGymClass("Cardio 1", "Introductory cardio", t3);
        GymClass g2 = createGymClass("Cardio 2", "Intermediate cardio", t2);
        GymClass g3 = createGymClass("Cardio 3", "Advanced cardio", t1);

        GymClass g4 = createGymClass("Strength training 1", "Introductory Strength training", t3);
        GymClass g5 = createGymClass("Strength training 2", "Intermediate Strength training", t2);
        GymClass g6 = createGymClass("Strength training 3", "Advanced Strength training", t1);

        Booking b1 = createBooking(c1, g1, t3, LocalDateTime.now().plusDays(1).withMinute(0).withSecond(0).withNano(0));
    }

    private Customer createCustomer(String firstName, String lastName) {
        Customer c = new Customer();
        c.setFirstName(firstName);
        c.setLastName(lastName);
        c.setAge(getRandomIntINRange(18, 80));
        c.setEmail(getRandomEmailAddress());
        return customerRepository.save(c);
    }

    private Trainer createTrainer(String firstName, String lastName) {
        Trainer t = new Trainer();
        t.setFirstName(firstName);
        t.setLastName(lastName);
        t.setAge(getRandomIntINRange(18, 80));
        t.setEmail(getRandomEmailAddress());
        return trainerRepository.save(t);
    }

    private GymClass createGymClass(String name, String description, Trainer trainer) {
        GymClass g = new GymClass();
        g.setName(name);
        g.setDescription(description);
        g.setDurationInHours(getRandomIntINRange(1, 3));
        g.setTrainer(trainer);
        return gymClassRepository.save(g);
    }

    private Booking createBooking(Customer c, GymClass g, Trainer t, LocalDateTime ts) {
        Booking b = new Booking(c, g, t, ts);
        return bookingRepository.save(b);
    }

    private int getRandomIntINRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    private String getRandomEmailAddress() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr + "@gmail.com";

    }
}
