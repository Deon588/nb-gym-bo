package com.nightsbridge.gym.dto;

import com.nightsbridge.gym.domain.Booking;

import java.time.LocalDateTime;

public class BookingDto {
    private Long id;
    LocalDateTime startTime;
    LocalDateTime endTime;
    Long gymClassId;
    String gymClassName;
    String gymClassDescription;
    String trainerName;

    public BookingDto(Long id, LocalDateTime startTime, LocalDateTime endTime, Long gymClassId, String gymClassName, String gymClassDescription, String trainerName) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.gymClassId = gymClassId;
        this.gymClassName = gymClassName;
        this.gymClassDescription = gymClassDescription;
        this.trainerName = trainerName;
    }

    public BookingDto(Booking b) {
        this.id = b.getId();
        this.startTime = b.getStartTime();
        this.endTime = b.getEndTime();
        this.gymClassId = b.getGymClass().getId();
        this.gymClassName = b.getGymClass().getName();
        this.gymClassDescription = b.getGymClass().getDescription();
        this.trainerName = b.getTrainer().getFirstName() + " " + b.getTrainer().getLastName();
    }

    public BookingDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Long getGymClassId() {
        return gymClassId;
    }

    public void setGymClassId(Long gymClassId) {
        this.gymClassId = gymClassId;
    }

    public String getGymClassName() {
        return gymClassName;
    }

    public void setGymClassName(String gymClassName) {
        this.gymClassName = gymClassName;
    }

    public String getGymClassDescription() {
        return gymClassDescription;
    }

    public void setGymClassDescription(String gymClassDescription) {
        this.gymClassDescription = gymClassDescription;
    }

    public String getTrainerName() {
        return trainerName;
    }

    public void setTrainerName(String trainerName) {
        this.trainerName = trainerName;
    }
}
