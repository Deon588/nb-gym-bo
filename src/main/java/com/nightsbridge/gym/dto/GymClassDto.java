package com.nightsbridge.gym.dto;

import com.nightsbridge.gym.domain.GymClass;

public class GymClassDto {
    private Long id;
    private String name;
    private String description;
    private int durationInHours;
    private String trainer;


    public GymClassDto(GymClass g) {
        this.id = g.getId();
        this.name = g.getName();
        this.description = g.getDescription();
        this.durationInHours = g.getDurationInHours();
        this.trainer = g.getTrainer().getFirstName() + " " + g.getTrainer().getLastName();
    }

    public GymClassDto(Long id, String name, String description, int durationInHours, String trainer) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.durationInHours = durationInHours;
        this.trainer = trainer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDurationInHours() {
        return durationInHours;
    }

    public void setDurationInHours(int durationInHours) {
        this.durationInHours = durationInHours;
    }

    public String getTrainer() {
        return trainer;
    }

    public void setTrainer(String trainer) {
        this.trainer = trainer;
    }
}
