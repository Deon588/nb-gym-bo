package com.nightsbridge.gym.service;

import com.nightsbridge.gym.dto.GymClassDto;

import java.util.List;

public interface GymClassService {
    public List<GymClassDto> getAllGymClasses();
    public List<GymClassDto> searchGymClasses(Long trainerId);
}
