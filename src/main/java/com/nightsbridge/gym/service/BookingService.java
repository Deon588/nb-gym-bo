package com.nightsbridge.gym.service;

import com.nightsbridge.gym.dto.BookingDto;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingService {
    public List<BookingDto> getAllBookings(Long userId);

    public BookingDto createBooking(Long gymClassId, LocalDateTime bookingStartTime);
}
