package com.nightsbridge.gym.service;

import com.nightsbridge.gym.domain.Trainer;
import com.nightsbridge.gym.dto.TrainerDto;
import com.nightsbridge.gym.repository.TrainerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class TrainerServiceImpl implements TrainerService {
    private final TrainerRepository trainerRepository;

    public TrainerServiceImpl(TrainerRepository trainerRepository) {
        this.trainerRepository = trainerRepository;
    }

    @Override
    public List<TrainerDto> getAllTrainers() {
        return trainerRepository.findAll()
                .stream()
                .map(TrainerDto::new)
                .collect(Collectors.toList());
    }


}
