package com.nightsbridge.gym.service;

import com.nightsbridge.gym.domain.Customer;
import com.nightsbridge.gym.repository.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    private final CustomerRepository customerRepository;

    public UserServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //returns current test user (purely for simplifying demo/assessment)
    @Override
    public Customer findCurrentTestUser() {
        return customerRepository.findByFirstName("Deon");
    }
}
