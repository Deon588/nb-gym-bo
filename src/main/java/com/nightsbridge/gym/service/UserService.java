package com.nightsbridge.gym.service;

import com.nightsbridge.gym.domain.Customer;

public interface UserService {
    public Customer findCurrentTestUser();
}
