package com.nightsbridge.gym.service;

import com.nightsbridge.gym.domain.*;
import com.nightsbridge.gym.dto.BookingDto;
import com.nightsbridge.gym.repository.BookingRepository;
import com.nightsbridge.gym.repository.CustomerRepository;
import com.nightsbridge.gym.repository.GymClassRepository;
import com.nightsbridge.gym.repository.TrainerRepository;
import com.querydsl.core.types.Predicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class BookingServiceImpl implements BookingService {
    private final CustomerRepository customerRepository;
    private final TrainerRepository trainerRepository;
    private final GymClassRepository gymClassRepository;
    private final BookingRepository bookingRepository;
    private final UserServiceImpl userService;


    public BookingServiceImpl(CustomerRepository customerRepository, TrainerRepository trainerRepository, GymClassRepository gymClassRepository, BookingRepository bookingRepository, UserServiceImpl userService) {
        this.customerRepository = customerRepository;
        this.trainerRepository = trainerRepository;
        this.gymClassRepository = gymClassRepository;
        this.bookingRepository = bookingRepository;
        this.userService = userService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<BookingDto> getAllBookings(Long userId) {
        return customerRepository.findById(userId)
                .get()
                .getBookings()
                .stream()
                .map(BookingDto::new)
                .collect(Collectors.toList());
    }


    @Override
    public BookingDto createBooking(Long gymClassId, LocalDateTime startTime) throws EntityExistsException {
        Customer customer = userService.findCurrentTestUser();
        GymClass gymClass = gymClassRepository.findById(gymClassId).get();
        Trainer trainer = gymClass.getTrainer();
        QBooking qBooking = QBooking.booking;
        LocalDateTime endTime = startTime.plusHours(gymClass.getDurationInHours());

        //check that none of the customer and trainer's existing bookings overlap with the proposed booking slot
        Predicate customerPred = qBooking.customer.eq(customer).and(qBooking.startTime.before(endTime)).and(qBooking.endTime.after(startTime));
        Predicate trainerPred = qBooking.trainer.eq(trainer).and(qBooking.startTime.before(endTime)).and(qBooking.endTime.after(startTime));

        Optional customerOverlappingBooking = bookingRepository.findOne(customerPred);
        Optional trainerOverlappingBooking = bookingRepository.findOne(trainerPred);

        if (customerOverlappingBooking.isPresent()) {
            throw new EntityExistsException("Customer with ID " + customer.getId().toString() + " has an existing booking that overlaps the proposed booking time please choose another time");
        }
        if (trainerOverlappingBooking.isPresent()) {
            throw new EntityExistsException("Trainer with ID " + trainer.getId().toString() + " has an existing booking that overlaps the proposed booking time please choose another time");
        }
        return new BookingDto(bookingRepository.saveAndFlush(new Booking(customer, gymClass, trainer, startTime)));
    }
}
