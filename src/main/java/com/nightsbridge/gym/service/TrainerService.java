package com.nightsbridge.gym.service;

import com.nightsbridge.gym.dto.TrainerDto;

import java.util.List;

public interface TrainerService {
    public List<TrainerDto> getAllTrainers();
}
