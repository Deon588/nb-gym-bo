package com.nightsbridge.gym.service;

import com.nightsbridge.gym.domain.QGymClass;
import com.nightsbridge.gym.dto.GymClassDto;
import com.nightsbridge.gym.repository.GymClassRepository;
import com.querydsl.core.types.Predicate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Transactional(readOnly = true)
public class GymClassServiceImpl implements GymClassService {
    private final GymClassRepository gymClassRepository;


    public GymClassServiceImpl(GymClassRepository gymClassRepository) {
        this.gymClassRepository = gymClassRepository;
    }

    @Override
    public List<GymClassDto> getAllGymClasses() {
        return gymClassRepository.findAll()
                .stream()
                .map(GymClassDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public List<GymClassDto> searchGymClasses(Long trainerId) {
        QGymClass gymClass = QGymClass.gymClass;
        Predicate p1 = gymClass.trainer.id.eq(trainerId);
        return StreamSupport.stream(gymClassRepository.findAll(p1).spliterator(), false)
                .map(GymClassDto::new)
                .collect(Collectors.toList());
    }
}