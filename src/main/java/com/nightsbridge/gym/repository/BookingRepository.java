package com.nightsbridge.gym.repository;

import com.nightsbridge.gym.domain.Booking;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Long>, QuerydslPredicateExecutor<Booking> {
}
