package com.nightsbridge.gym.repository;

import com.nightsbridge.gym.domain.GymClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface GymClassRepository extends JpaRepository<GymClass, Long>, QuerydslPredicateExecutor<GymClass> {
}
