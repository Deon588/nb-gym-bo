package com.nightsbridge.gym;

import com.nightsbridge.gym.controller.GymClassController;
import com.nightsbridge.gym.domain.GymClass;
import com.nightsbridge.gym.dto.GymClassDto;
import com.nightsbridge.gym.repository.GymClassRepository;
import com.nightsbridge.gym.service.BookingService;
import com.nightsbridge.gym.service.GymClassService;
import com.nightsbridge.gym.service.GymClassServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(GymClassController.class)
@AutoConfigureRestDocs(outputDir = "build/snippets")
public class GymClassControllerTests {

    @TestConfiguration
    public class testConfig {
        @Bean
        public GymClassRepository gymClassRepository() {
            return Mockito.mock(GymClassRepository.class);
        }

    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GymClassServiceImpl gymClassService;

    @Test
    public void when_inputIsValid__expect_getAllGymClassesToReturnListOfBookingDtos() {
        GymClassDto gd1 = new GymClassDto(1L, "D", "n", 1, "t1");
        GymClassDto gd2 = new GymClassDto(2L, "D", "d", 1, "t2");
        List<GymClassDto> gList = new ArrayList<>();
        gList.add(gd1);
        gList.add(gd2);

        given(gymClassService.getAllGymClasses()).willReturn(gList);

        try {
    mockMvc.perform(get("/nb/gymclasses"))
            .andExpect(status().isOk())
            .andDo(document("{method-name}/",
                    preprocessResponse(prettyPrint()),
                    responseFields(
                            fieldWithPath("id").description("The gym class ID"),
                            fieldWithPath("name").description("The gym class name"),
                            fieldWithPath("durationInHours").description("The gym class duration"),
                            fieldWithPath("trainer").description("The gym class trainer's first and second names")
                    )
            ));
} catch (Exception e) {
    System.out.println(e.getMessage());
        }
    }
@Test
    public void when_inputIsValid_expect_searchGymClassesToReturnListOfFilteredBookingDtos() {
        GymClassDto gd1 = new GymClassDto(1L, "D", "n", 1, "t1");
        GymClassDto gd2 = new GymClassDto(2L, "D", "d", 1, "t1");

        List<GymClassDto> gList = new ArrayList<>();

        gList.add(gd1);
        gList.add(gd2);

        given(gymClassService.searchGymClasses(1L)).willReturn(gList);

        try {
            mockMvc.perform(get("/nb/gymclasses/search/1"))
                    .andExpect(status().isOk())
                    .andDo(document("{method-name}/",
                            preprocessResponse(prettyPrint()),
                            pathParameters(
                                    parameterWithName("trainerId").description("The trainer ID to filter on")
                            ),
                            responseFields(
                                    fieldWithPath("id").description("The gym class ID"),
                                    fieldWithPath("name").description("The gym class name"),
                                    fieldWithPath("durationInHours").description("The gym class duration"),
                                    fieldWithPath("trainer").description("The gym class trainer's first and second names")
                            )
                    ));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
